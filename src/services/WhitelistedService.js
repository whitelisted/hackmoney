import ENV from '../env';

const ROUTES = {
  ADDRESS: 'api/address'
};

const DEFAULT_HEADERS = {
  'Content-Type': 'application/json'
};

const createUrl = (routes = [], params = []) => {
  const envName = process.env.NODE_ENV;
  console.log('envName', envName);

  return `${ENV[envName].BACKEND_DOMAIN}/${routes.join('/')}?${params.join('&')}`;
}

export default class WhitelistedService {
  static async validateAddress(address, transactionHash = "") {
    const url = createUrl(
      [ROUTES.ADDRESS, address]
    );

    const data = {
      transaction_hash: transactionHash
    };

    return fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: DEFAULT_HEADERS
    })
      .then((response) => response.json())
  }
}
