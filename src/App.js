import { useState, useEffect } from 'react';
import Nav from './components/nav/Nav';
import Main from './components/main/Main';
import Results from './components/results/Results';
import WhitelistedService from './services/WhitelistedService';
import './App.scss';

function App() {
  const [ searchAddress, setSearchAddress ] = useState('');
  const [ fetching, setFetching ] = useState(false);
  const [ addressLegit, setAddressLegit ] = useState(null);

  const onSearchClick = async () => {
    setFetching(true);

    const response = await WhitelistedService.validateAddress(searchAddress);
    setFetching(false);
    setAddressLegit(response.result === "OK");
  };

  useEffect(() => {
    setAddressLegit(null);
  }, [searchAddress]);

  return (
    <div className="App">
      <Nav />
      <Main 
        searchAddress={ searchAddress }
        setSearchAddress={ setSearchAddress }
        fetching={ fetching }
        onSearchClick={ onSearchClick }
      />
      <Results 
        searchAddress={ searchAddress }
        addressLegit={ addressLegit }
      />
    </div>
  );
}

export default App;
