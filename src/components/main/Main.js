import Loader from '../loader/Loader';

import './Main.scss';

const DEFAULT_BUTTON_TEXT = 'Search';
const ENTER_KEY_CODE = 'Enter';

export default function Main(props) {
  const isButtonDisabled = props.fetching || props.searchAddress.length === 0;

  const onKeyPress = (e) => {
    if (e.code === ENTER_KEY_CODE) {
      props.onSearchClick();
    }
  };

  return (
    <div className="main-container">
      <div className="element">
        <label className="paste-label">Paste the address here</label>
      </div>
      <div className="element">
        <input
          type="text"
          placeholder="0x"
          className="address-input"
          disabled={ props.fetching }
          value={ props.searchAddress }
          onChange={ (e) => props.setSearchAddress(e.target.value) }
          onKeyPress={ onKeyPress }
        />
      </div>
      <div className="element">
        <button
          className={`search-button ${isButtonDisabled ? 'search-button-clicked' : '' }`}
          onClick={ props.onSearchClick }
          disabled={ isButtonDisabled }
        >
          {
            props.fetching ?
              <Loader /> :
              DEFAULT_BUTTON_TEXT
          }
        </button>
      </div>
    </div>
  );
}
