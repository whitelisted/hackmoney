// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./RootBlacklistRegistry.sol";
import "./BlacklistDirectoriesRegistry.sol";

contract Whitelisted is Ownable, RootBlacklistRegistry, BlacklistDirectoriesRegistry {
  using SafeMath for uint256;
  
  constructor() {
    
  }

  function addToRootBlacklist(address addr) external override onlyOwner returns (bool) {
    if (rootBlacklistMapping[addr] == true) {
          return false;
    }
    rootBlacklist.push(addr); 
    rootBlacklistMapping[addr]=true;
    return true;
  }

  function addBlacklistDirectory(bytes calldata directory) external override onlyOwner returns (bool) {
    directories.push(directory);
    return true;
  }
    
}
