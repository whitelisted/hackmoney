pragma solidity ^0.8.0;

abstract contract BlacklistDirectoriesRegistry {
    
    bytes[] public directories;

    constructor() {
    }

    function addBlacklistDirectory(bytes calldata) external virtual returns (bool);
    

    function directoriesCount() external view returns (uint) {
        return directories.length;
    }

    
}