pragma solidity ^0.8.0;

abstract contract RootBlacklistRegistry {
    mapping (address => bool) rootBlacklistMapping;

    address[] public rootBlacklist;

    constructor() {
    }

    function addToRootBlacklist(address) external virtual returns (bool);
    
    function isBlacklisted(address addr) external view returns (bool) {
        return rootBlacklistMapping[addr];
    }

    function rootBlacklistCount() external view returns (uint) {
        return rootBlacklist.length;
    }

    
}