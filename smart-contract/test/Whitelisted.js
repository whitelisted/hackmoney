const WhitelistedSmartContract = artifacts.require("./Whitelisted");
const rootBlacklist = require('../root-blacklist-test');
const BN = web3.utils.BN;

const EMPTY_FILE_IPFS_HASH='QmWgRdVXgHmiBxai5m9henjdS9hcoWQ7x3TWqZpKobKPD8';

async function assertThrow(promise) {
  try {
    await promise;
  } catch (error) {
    return
  }
  assert.fail('Expected throw but no error.');
}

let whitelistedInstance;
/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract('Whitelisted', ([owner, account1, account2]) =>  {

  async function deploy() {
    whitelistedInstance = await WhitelistedSmartContract.new({from: owner});
  }

  async function updateRootBlacklist() {
    for (let darkAddress of rootBlacklist) {
        await whitelistedInstance.addToRootBlacklist(darkAddress, {from: owner});
    };
  }

  describe('Test RootBlacklistRegistry', async () => {

    before(async() => {
      await deploy();
      await updateRootBlacklist();
    });
  

    it("Should check root blacklist consistency", async function () {
      const blacklistCount = await whitelistedInstance.rootBlacklistCount();
      for (let i = new BN(0); i.lt(blacklistCount); i.iadd(new BN(1))) {
        const addr = await whitelistedInstance.rootBlacklist(i);
        assert.ok(rootBlacklist.find(a => a == addr));
        const isBlacklisted = await whitelistedInstance.isBlacklisted(addr);
        assert.equal(isBlacklisted, true);
      }
    });

    it("Should be marked as (root)blacklisted", async function () {
      const isBlacklisted = await whitelistedInstance.isBlacklisted(rootBlacklist[0]);
      assert.equal(isBlacklisted, true);
    });

    it("Should NOT be marked as (root)blacklisted", async function () {
      const isBlacklisted = await whitelistedInstance.isBlacklisted(owner);
      assert.equal(isBlacklisted, false);
    });

    it('Should allow adding to (root)blacklist', async function () {
      await whitelistedInstance.addToRootBlacklist(account1, {from: owner});
      const isBlacklisted = await whitelistedInstance.isBlacklisted(account1);
      assert.equal(isBlacklisted, true);
    });

    it('Should NOT allow adding to (root)blacklist', async function () {
      await assertThrow(whitelistedInstance.addToRootBlacklist(account2, {from: account1}));
    });
  });


  describe('Test BlacklistDirectoriesRegistry', async () => {

    beforeEach(async() => {
      await deploy();
    });

    it("Should allow adding a blacklist directory", async function () {
      await whitelistedInstance.addBlacklistDirectory(web3.utils.asciiToHex(EMPTY_FILE_IPFS_HASH), {from: owner});
    });


    it("Should NOT allow adding a blacklist directory", async function () {
      await assertThrow(whitelistedInstance.addBlacklistDirectory(web3.utils.asciiToHex(EMPTY_FILE_IPFS_HASH), {from: account1}));
    });
  

    it("Should check blacklist directories consistency", async function () {
      const expectedDirectories = [EMPTY_FILE_IPFS_HASH];
      await whitelistedInstance.addBlacklistDirectory(web3.utils.asciiToHex(EMPTY_FILE_IPFS_HASH), {from: owner});
      const directoriesCount = await whitelistedInstance.directoriesCount();
      for (let directory of expectedDirectories) { // For each expected directory
        let found = false;
        for (let i = new BN(0); i.lt(directoriesCount); i.iadd(new BN(1))) { // Try finding it on blockchain
          const blockchainDirHex = await whitelistedInstance.directories(i);
          assert.ok(blockchainDirHex);
          const blockchainDirAscii = web3.utils.hexToAscii(blockchainDirHex);
          assert.ok(blockchainDirAscii);
          if (blockchainDirAscii == directory) {
            found = true;
            break;
          }
        }
        if ( found == false) {
          assert.fail('Directory ' + directory + 'was not found on blockchain');
        }
      }
    });

  });

});
