require("dotenv").config();
const _ = require('lodash');

const WhitelistedSmartContract = artifacts.require("./Whitelisted");
const rootBlacklistTest = require('../root-blacklist-test');
const rootBlacklistMainnet = require('../root-blacklist');

const NETWORK_DEV = 'development';
const NETWORK_ROPSTEN = 'ropsten';
const NETWORK_MAINNET = 'mainnet';

function getBlacklist(network) {
    switch (network) {
        case NETWORK_MAINNET:
             return rootBlacklistMainnet;
             break;
        case NETWORK_ROPSTEN:
        case NETWORK_DEV:
        default:
             return rootBlacklistTest;
    }
}

module.exports = async function (deployer, network, accounts) {
    const owner = _.first(accounts);

    await deployer.deploy(WhitelistedSmartContract);
    const whitelistedInstance = await WhitelistedSmartContract.deployed();
    console.log('Whitelisted contract deployed.');
    
    // Only update root blacklist on dev environment
    if (network === NETWORK_DEV) {
        console.log('Setting up root blacklist...');
        const rootBlacklist = getBlacklist(network);
            for (let darkAddress of rootBlacklist) {
                console.log('Adding ',darkAddress);
                await whitelistedInstance.addToRootBlacklist(darkAddress, {from: owner});
            };
        console.log('Root blacklist updated.');
    }
    
};