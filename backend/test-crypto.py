from crypto import aes_256_cbc_encrypt
from crypto import aes_256_cbc_decrypt
import binascii

message = "blacklist={0x95..., 0x3151...,...}"
print ('initial plaintext: '+ message)
ctxt = aes_256_cbc_encrypt(message)
print (ctxt)

plain = aes_256_cbc_decrypt(ctxt)
print (plain)