## Running the backend 

### build the docker image
`docker build --tag python-docker .`

### prepare enviroment variables
1. `cp env.example env`
2. fill the required variables in the newly created file **env**

### Run it
`docker run --env-file env -p 5000:5000 python-docker`

Now you can see it running on http://127.0.0.1:5000/
