from os import urandom
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
import binascii
import codecs
import os

secret_key = urandom(32)
iv = urandom(16)

if(os.path.exists("key")):
   f = open("key","rb")
   secret_key = f.read(32)
else:
    f = open("key","wb")
    f.write(secret_key)
    
if(os.path.exists("iv")):
   f = open("iv","rb")
   iv = f.read(16)
else:
    f = open("iv","wb")
    f.write(iv)



def aes_256_cbc_encrypt(message):
    obj = AES.new(secret_key, AES.MODE_CBC, iv)
    m=codecs.encode(message,'UTF-8')
    padded = pad(m,16)
    bytepadded = codecs.decode(padded,'UTF-8')
    return obj.encrypt(bytepadded.encode("UTF-8"))

def aes_256_cbc_decrypt(ciphertext):
    obj = AES.new(secret_key, AES.MODE_CBC, iv)
    dec = obj.decrypt(ciphertext)
    unpadded = unpad(dec,16)
    return unpadded.decode("UTF-8")
