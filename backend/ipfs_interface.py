import json

import requests
import os

from web3_interface import get_directories

print(os.environ)
INFURA_KEY = os.environ['INFURA_KEY']
INFURA_SECRET = os.environ['INFURA_SECRET']

# TODO: IPFS directories contents need to be encrypted
BLACK_LISTS = get_directories()


def test_connection():
    print("trying IPFS..")
    files = {'file': ('WhiteListed')}
    response = requests.post('https://ipfs.infura.io:5001/api/v0/add', files=files, auth=(INFURA_KEY, INFURA_SECRET))
    print("status: ", response.status_code, " Body: ", response.json())


test_connection()


def check_in_black_list(address):
    for black_list in BLACK_LISTS:
        response = requests.post('https://ipfs.infura.io:5001/api/v0/cat', # or /api/v0/block/get
                                 params=(("arg", black_list),),
                                 auth=(INFURA_KEY, INFURA_SECRET))
        print("inspecting black list ", black_list, ", status: ", response.status_code)
        content = json.loads(response.text.strip("'<>() ").replace('\'', '\"'))
        if address in content:
            return {"result": "BLACK_LIST", "interactions": [
                {black_list: [{"address": address, "comment": None, "date": None}]}]}


    return {"result": "OK", "interactions": []}
