from flask import Flask, request, jsonify
from flask_cors import CORS

from ipfs_interface import check_in_black_list
from web3_interface import verify_payment

app = Flask(__name__)
CORS(app)
cors = CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})

@app.route("/api")
def index():
    return "WhiteListed"

@app.route("/api/address/<address>", methods=['POST'])
def address_verification(address):
    content = request.json
    print("Received request content: ", content)

    #verify_payment(content["transaction_hash"])

    result = check_in_black_list(address)

    print("result: ", result)

    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
