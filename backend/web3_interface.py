import json
import os

from web3 import Web3

INFURA_ETH = os.environ['INFURA_ETH']
CONTRACT_ADDRESS = "0x5EE91E41c32587B66A58047D1Ce69419A74fd4E9"

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

abi = ""
with open(os.path.join(ROOT_DIR, "abis/Whitelisted.json")) as f:
    abi = json.load(f)
print("abi: ", abi)

w3 = Web3(Web3.HTTPProvider(INFURA_ETH))
contract = w3.eth.contract(address=CONTRACT_ADDRESS, abi=json.dumps(abi))

def get_directories():
    dirs = []
    directories_count = contract.functions.directoriesCount().call()
    for d in range(directories_count):
        hex_dir = contract.functions.directories(d).call()
        dirs.append(Web3.toText(hex_dir))
    return dirs


def verify_payment(transaction_hash):
    # TODO, needs more details, to be implemented after the smart contract
    pass
